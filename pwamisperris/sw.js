var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
  '/',
  '/css/bootstrap.css',
  '/css/estilos.css',
  '/js/app.js',
  '/js/bootstrap.js',
  '/js/jquery-3.3.1.js',
  '/js/popper.js',
  'img/page/logo.png',
  'img/imagenes/adoptados/Apolo.jpg',
  'img/imagenes/adoptados/Duque.jpg',
  'img/imagenes/adoptados/Tom.jpg',
  'img/page/rescate.jpg',
  'img/page/crowfunding.jpg',
  'img/imagenes/rescatados/Bigotes_tn.jpg',
  'img/imagenes/rescatados/Chocolate_tn.jpg',
  'img/imagenes/rescatados/Luna_tn.jpg',
  'img/imagenes/rescatados/Maya_tn.jpg',
  'img/imagenes/rescatados/Oso_tn.jpg',
  'img/imagenes/rescatados/Pexel_tn.jpg',
  'img/imagenes/rescatados/Wifi_tn.jpg',
  'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Cache open!');
        return cache.addAll(urlsToCache);
      })
  );
});


self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            // Cache hit - return response
            if (response) {
                return response;
            }
            return fetch(event.request);
        })
    );
});

// self.addEventListener('activate', event => {
//     // remove old caches
//     event.waitUntil(
//       caches.keys().then(keys => Promise.all(
//         keys.map(key => {
//           if (key != CACHE_NAME) {
//             return caches.delete(key);
//           }
//         })
//       )).then(() => {
//         console.log('Now ready to handle fetches!');
//       })
//     );
// });


