from django.db import models

# Create your models here.

class Persona(models.Model):
    correo = models.EmailField(max_length=100)
    run = models.CharField(max_length=10)
    nombrePersona = models.CharField(max_length=200)
    fnacimiento = models.DateField()
    telefono = models.IntegerField()
    region = models.CharField(max_length=100)
    comuna = models.CharField(max_length=100)
    vivienda = models.CharField(max_length=100)
    contrasenia = models.CharField(max_length=40)
    def __str__(self):
        return " run: "+self.run+"nombre: "+self.nombrePersona
    
    # def __str__(self):
    #     return "correo: "+self.correo+" run: "+self.run+"nombre: "+self.nombre+" fnacimiento: "+self.fnacimiento+" telefono: "+self.telefono+" region: "+self.region+" comuna: "+self.comuna+" vivienda: "+self.vivienda

class Perro(models.Model):
    id = models.AutoField(primary_key=True)
    foto = models.ImageField(upload_to="registro/imagenes/")
    nombrePerro = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    estado = models.CharField(max_length=100)
    def __str__(self):
        return "nombrePerro: "+self.nombrePerro

    # def __str__(self):
    #     return "foto: "+self.foto+"nombrePerro: "+self.nombrePerro+" raza: "+self.raza+" descripcion: "+self.descripcion+" estado: "+self.estado