from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Persona, Perro

import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from .serializer import PerroSerializer
from rest_framework import viewsets

# Create your views here.

def index(request):
    return render(request,'index.html',{'Perro':Perro.objects.all()})

def registro(request):
    return render(request,'formulario.html',{})

def login(request):
    return render(request,'login.html',{})

#metodo de persona
def crearPersona(request):
    correo = request.POST.get('correo','')
    run = request.POST.get('run','')
    nombrePersona = request.POST.get('nombrePersona','')
    fnacimiento = request.POST.get('fnacimiento','')
    telefono = request.POST.get('telefono',0)
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    vivienda = request.POST.get('vivienda','')
    contrasenia = request.POST.get('contrasenia','')
    persona = Persona(correo=correo, run=run, nombrePersona=nombrePersona, fnacimiento=fnacimiento, telefono=telefono, region=region, comuna=comuna, vivienda=vivienda,contrasenia=contrasenia)
    persona.save()
    user=User.objects.create_user(username=persona.correo,password=persona.contrasenia)
    user.save()
    return redirect("index")

# metodos de login
def login_iniciar(request):
    correo = request.POST.get('correo','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=correo, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        return redirect("index")
    else:
        return redirect("login")

@login_required(login_url='login')
def cerrar_session(request):
    logout(request)
    return redirect('index')

# metodos para los perros
@login_required(login_url='login')
@staff_member_required(login_url='login')  
def crearPerro(request):
    foto = request.FILES.get('foto',False)
    nombrePerro = request.POST.get('nombrePerro','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perro = Perro(foto=foto, nombrePerro=nombrePerro, raza=raza, descripcion=descripcion, estado=estado)
    perro.save()
    return redirect("index")

@login_required(login_url='login')
@staff_member_required(login_url='login')  
def editarPerro(request,id):
    perro = Perro.objects.get(pk=id)
    return render(request,'editarPerro.html',{'perro':perro})

@login_required(login_url='login')
@staff_member_required(login_url='login')  
def editadoPerro(request,id):
    perro = Perro.objects.get(pk=id)

    foto = request.FILES.get('foto',False)
    nombrePerro = request.POST.get('nombrePerro','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')

    perro.foto = foto
    perro.nombrePerro = nombrePerro
    perro.raza = raza
    perro.descripcion = descripcion
    perro.estado = estado
    perro.save()
    return redirect('index')

@login_required(login_url='login')
@staff_member_required(login_url='login')  
def eliminarPerro(request,id):
    perro = Perro.objects.get(pk=id)
    perro.delete()
    return redirect('index')

class PerroViewSet(viewsets.ModelViewSet):
    queryset = Perro.objects.all()
    serializer_class = PerroSerializer