from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'perro', views.PerroViewSet)

urlpatterns = [
    path('',views.index,name="index"),

    path('registro/',views.registro,name="registro"),

    path('registro/crearPersona',views.crearPersona,name="crearPersona"),
    
    path('registro/crearPerro',views.crearPerro,name="crearPerro"),
    path('registro/editarPerro/<int:id>',views.editarPerro,name="editarPerro"),
    path('registro/editadoPerro/<int:id>',views.editadoPerro,name="editadoPerro"),
    path('registro/eliminarPerro/<int:id>',views.eliminarPerro,name="eliminarPerro"),

    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    path('login/',views.login,name="login"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('login/iniciar',include('social_django.urls',namespace='iniciar')),

    path('password/recovery/',auth_views.PasswordResetView.as_view(template_name='auth/password_reset_form.html',html_email_template_name='auth/password_reset_email.html',),name='password_reset'),
    path('password/recovery/done/$',auth_views.PasswordResetDoneView.as_view(template_name='auth/password_reset_done.html',),name='password_reset_done'),
    path('password/recovery/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',auth_views.PasswordResetConfirmView.as_view(success_url=reverse_lazy('login'),post_reset_login=True,template_name='auth/password_reset_confirm.html',post_reset_login_backend=('django.contrib.auth.backends.AllowAllUsersModelBackend'),),name='password_reset_confirm',),
    
    path('api/',include(router.urls))
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)